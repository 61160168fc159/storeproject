/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author KoonAoN
 */
public class TestInsertProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "INSERT INTO product(Prod_Name,Prod_Price)VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            Product product = new Product(-1,"Oh Leing",20);
            stmt.setString(1,product.getProd_Name());
            stmt.setDouble(2, product.getProd_Price());
            int row = stmt.executeUpdate();
            System.out.println("Affect Row"+ row);
            ResultSet result = stmt.getGeneratedKeys();
            int Prod_ID = -1;
            if(result.next()){
                Prod_ID = result.getInt(1);
            }
            System.out.println("Affect Row "+ row +" Prod_ID :"+ Prod_ID);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
    }
}
