/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;

/**
 *
 * @author KoonAoN
 */
public class TestReceipt {

    public static void main(String[] args) {
        Product p1 = new Product(1, "tea", 30);
        Product p2 = new Product(2, "green tea", 35);
        Employee seller = new Employee("SRKA", "0617214066", "password");
        Customer customer = new Customer("AON", "0000000000");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 5);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p2, 5);
        System.out.println(receipt);
    }

}
