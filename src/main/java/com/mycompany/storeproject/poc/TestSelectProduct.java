/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author KoonAoN
 */
public class TestSelectProduct {

    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        try {
            String sql = "SELECT Prod_ID,Prod_Name,Prod_Price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()){              
                int Prod_ID = result.getInt("Prod_ID");
                String Prod_Name = result.getString("Prod_Name");
                double Prod_Price = result.getDouble("Prod_Price");
                Product product = new Product(Prod_ID,Prod_Name,Prod_Price);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
    } 
}
