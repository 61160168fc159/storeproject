/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author KoonAoN
 */
public class Database {

    private static Database instance = new Database();

    private Connection conn;

    private Database() {

    }

    public static Database getInstance() {
        String dbPath = "./db/store.db";
        try {
            if (instance.conn == null || instance.conn.isClosed()) {
                Class.forName("org.sqlite.JDBC");
                instance.conn = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
                System.out.println("Database Connection");
            }
        } catch (ClassNotFoundException ex) {
            System.out.print("Error : Class JDBC is not Exist");
        } catch (SQLException ex) {
            System.out.print("Error : Database Cannot Connection");
        }
        return instance;
    }

    public static void close() {
        try {
            if (instance.conn != null && !instance.conn.isClosed()) {
                instance.conn.close();
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.conn = null;
    }
    public Connection getConnection(){
        return instance.conn;
    }
}
