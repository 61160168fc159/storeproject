/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author KoonAoN
 */
public class Employee {
    private int Emp_ID;
    private String Emp_Name;
    private String Emp_Tel;
    private String Emp_Password;
    
    public Employee(int Emp_ID,String Emp_Name,String Emp_Tel,String Emp_Password){
        this.Emp_ID = Emp_ID;
        this.Emp_Name = Emp_Name;
        this.Emp_Tel = Emp_Tel;
        this.Emp_Password = Emp_Password;
    }

    public Employee(String Emp_Name, String Emp_Tel, String Emp_Password) {
       this(-1,Emp_Name,Emp_Tel,Emp_Password);
    }
    
    public Employee(int Emp_ID,String Emp_Name, String Emp_Tel) {
       this(Emp_ID,Emp_Name,Emp_Tel,"");
    }
    
    public Employee(String Emp_Name, String Emp_Tel) {
       this(-1,Emp_Name,Emp_Tel,"");
    }
    
    public int getEmp_ID() {
        return Emp_ID;
    }

    public void setEmp_ID(int Emp_ID) {
        this.Emp_ID = Emp_ID;
    }

    public String getEmp_Name() {
        return Emp_Name;
    }

    public void setEmp_Name(String Emp_Name) {
        this.Emp_Name = Emp_Name;
    }

    public String getEmp_Tel() {
        return Emp_Tel;
    }

    public void setEmp_Tel(String Emp_Tel) {
        this.Emp_Tel = Emp_Tel;
    }

    public String getEmp_Password() {
        return Emp_Password;
    }

    public void setEmp_Password(String Emp_Password) {
        this.Emp_Password = Emp_Password;
    }

    @Override
    public String toString() {
        return "Employee{" + "Emp_ID=" + Emp_ID + ", Emp_Name=" + Emp_Name + ", Emp_Tel=" + Emp_Tel + ", Emp_Password=" + Emp_Password + '}';
    }
    
    
}
