/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author KoonAoN
 */
public class Receipt {

    private int Rec_ID;
    private Date Created;
    private Employee Seller;
    private Customer Customer;
    private ArrayList<ReceiptDetail> receiptDetail;

    public Receipt(int Rec_ID, Date Created, Employee Seller, Customer Customer) {
        this.Rec_ID = Rec_ID;
        this.Created = Created;
        this.Seller = Seller;
        this.Customer = Customer;
        receiptDetail = new ArrayList<>();
    }

    public Receipt(Employee Seller, Customer Customer) {
        this(-1, null, Seller, Customer);
    }

    public void addReceiptDetail(int Rec_ID, Product product, int amount, double recDet_Price) {
        for(int row = 0;row<receiptDetail.size();row++){
            ReceiptDetail r = receiptDetail.get(row);
            if(r.getProduct().getProd_ID() == product.getProd_ID()){
                r.addAmount(amount);
                return;
            }
        }
        receiptDetail.add(new ReceiptDetail(Rec_ID, product, amount, recDet_Price, this));
    }

    public void addReceiptDetail(Product product, int amount) {
        addReceiptDetail(-1, product, amount,product.getProd_Price());
    }
    
    public void deleteReceiptDetail(int row){
        receiptDetail.remove(row);
    }
    
    public double getTotal(){
        double total = 0;
        for(ReceiptDetail r:receiptDetail){
            total = total + r.getTotal();
        }
        return total;
    }
    public int getRec_ID() {
        return Rec_ID;
    }

    public void setRec_ID(int Rec_ID) {
        this.Rec_ID = Rec_ID;
    }

    public Date getCreated() {
        return Created;
    }

    public void setCreated(Date Created) {
        this.Created = Created;
    }

    public Employee getSeller() {
        return Seller;
    }

    public void setSeller(Employee Seller) {
        this.Seller = Seller;
    }

    public Customer getCustomer() {
        return Customer;
    }

    public void setCustomer(Customer Customer) {
        this.Customer = Customer;
    }

    public ArrayList<ReceiptDetail> getReceiptDetail() {
        return receiptDetail;
    }

    public void setReceiptDetail(ArrayList<ReceiptDetail> ReceiptDetail) {
        this.receiptDetail = receiptDetail;
    }

    @Override
    public String toString() {
        String str = "Receipt{" + "Rec_ID=" + Rec_ID 
                + ", Created=" + Created 
                + ", Seller=" + Seller 
                + ", Customer=" + Customer
                +", Total="+this.getTotal()
                + "}\n";
        for(ReceiptDetail r:receiptDetail){
            str = str + r.toString()+"\n";
        }
        return str;
    }
    
}
