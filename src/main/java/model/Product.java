/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author KoonAoN
 */
public class Product {
    private int Prod_ID;
    private String Prod_Name;
    private double Prod_Price;

    public Product(int Prod_ID, String Prod_Name, double Prod_Price) {
        this.Prod_ID = Prod_ID;
        this.Prod_Name = Prod_Name;
        this.Prod_Price = Prod_Price;
    }

    public int getProd_ID() {
        return Prod_ID;
    }

    public void setProd_ID(int Prod_ID) {
        this.Prod_ID = Prod_ID;
    }

    public String getProd_Name() {
        return Prod_Name;
    }

    public void setProd_Name(String Prod_Name) {
        this.Prod_Name = Prod_Name;
    }

    public double getProd_Price() {
        return Prod_Price;
    }

    public void setProd_Price(double Prod_Price) {
        this.Prod_Price = Prod_Price;
    }

    @Override
    public String toString() {
        return "Product{" + "Prod_ID=" + Prod_ID + ", Prod_Name=" + Prod_Name + ", Prod_Price=" + Prod_Price + '}';
    }
    
}

