/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author KoonAoN
 */
public class Customer {
    private int Cus_ID;
    private String Cus_Name;
    private String Cus_Tel;
    
    public Customer(int Cus_ID,String Cus_Name,String Cus_Tel){
        this.Cus_ID = Cus_ID;
        this.Cus_Name = Cus_Name;
        this.Cus_Tel = Cus_Tel;
    }

    public Customer(String Cus_Name, String Cus_Tel) {
        this(-1,Cus_Name,Cus_Tel);
    }
    
    public int getCus_ID() {
        return Cus_ID;
    }

    public void setCus_ID(int Cus_ID) {
        this.Cus_ID = Cus_ID;
    }

    public String getCus_Name() {
        return Cus_Name;
    }

    public void setCus_Name(String Cus_Name) {
        this.Cus_Name = Cus_Name;
    }

    public String getCus_Tel() {
        return Cus_Tel;
    }

    public void setCus_Tel(String Cus_Tel) {
        this.Cus_Tel = Cus_Tel;
    }

    @Override
    public String toString() {
        return "Customer{" + "Cus_ID=" + Cus_ID + ", Cus_Name=" + Cus_Name + ", Cus_Tel=" + Cus_Tel + '}';
    }
    
    
}
