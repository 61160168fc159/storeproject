/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author KoonAoN
 */
public class ReceiptDetail {

    private int recDet_ID;
    private Product product;
    private int recDet_Amount;
    private double recDet_Preice;
    private Receipt receipt;

    public ReceiptDetail(int recDet_ID, Product product, int recDet_Amount, double recDet_Price, Receipt receipt) {
        this.recDet_ID = recDet_ID;
        this.product = product;
        this.recDet_Amount = recDet_Amount;
        this.recDet_Preice = recDet_Preice;
        this.receipt = receipt;
    }

    public ReceiptDetail(Product product, int recDet_Amount, double recDet_Price, Receipt receipt) {
        this(-1, product, recDet_Amount, recDet_Price, receipt);
    }

    public double getTotal() {
        return recDet_Amount * recDet_Preice;
    }

    public int getRecDet_ID() {
        return recDet_ID;
    }

    public void setRecDet_ID(int recDet_ID) {
        this.recDet_ID = recDet_ID;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getRecDet_Amount() {
        return recDet_Amount;
    }

    public void setRecDet_Amount(int recDet_Amount) {
        this.recDet_Amount = recDet_Amount;
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public double getRecDet_Preice() {
        return recDet_Preice;
    }

    public void setRecDet_Preice(double recDet_Preice) {
        this.recDet_Preice = recDet_Preice;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }
    
    public void addAmount(int amount){
        this.recDet_Amount = recDet_Amount + amount;
    }
    @Override
    public String toString() {
        return "ReceiptDetail{" + "recDet_ID=" + recDet_ID 
                + ", product=" + product 
                + ", recDet_Amount=" + recDet_Amount 
                + ", recDet_Preice=" + recDet_Preice 
                +", total="+ this.getTotal()
                + '}';
    }
    
}
