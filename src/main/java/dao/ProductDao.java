/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author KoonAoN
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int Prod_ID = -1;
        try {
            String sql = "INSERT INTO product(Prod_Name,Prod_Price)VALUES(?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getProd_Name());
            stmt.setDouble(2, object.getProd_Price());
            int row = stmt.executeUpdate();
            System.out.println("Affect Row" + row);
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                Prod_ID = result.getInt(1);
            }
            return Prod_ID;
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return -1;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Prod_ID,Prod_Name,Prod_Price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int Prod_ID = result.getInt("Prod_ID");
                String Prod_Name = result.getString("Prod_Name");
                double Prod_Price = result.getDouble("Prod_Price");
                Product product = new Product(Prod_ID, Prod_Name, Prod_Price);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int Prod_ID) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Prod_ID,Prod_Name,Prod_Price FROM product WHERE Prod_ID = " + Prod_ID;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pProd_ID = result.getInt("Prod_ID");
                String Prod_Name = result.getString("Prod_Name");
                double Prod_Price = result.getDouble("Prod_Price");
                Product product = new Product(pProd_ID, Prod_Name, Prod_Price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int Prod_ID) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE Prod_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Prod_ID);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int row = 0;
        try {
            String sql = "UPDATE product SET Prod_Name = ?,Prod_Price = ? WHERE Prod_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getProd_Name());
            stmt.setDouble(2, object.getProd_Price());
            stmt.setInt(3, object.getProd_ID());
            row = stmt.executeUpdate();
            System.out.println("Affect Row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "Coffe", 30));
        System.out.println("id : " + id);
        Product lastProduct = dao.get(id);
        System.out.println("Last Product : "+lastProduct);
        lastProduct.setProd_Price(100);
        int row = dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println("Update Product : "+updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("Delete Product : "+deleteProduct);
    }

}
