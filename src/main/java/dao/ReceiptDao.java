/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Employee;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;

/**
 *
 * @author KoonAoN
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int Bill_ID = -1;
        try {
            String sql = "INSERT INTO bill (Cus_ID,Emp_ID,Bill_Total) VALUES (?,?,?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, object.getCustomer().getCus_ID());
            stmt.setInt(2, object.getSeller().getEmp_ID());
            stmt.setDouble(3, object.getTotal());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();
            if (result.next()) {
                Bill_ID = result.getInt(1);
                object.setRec_ID(Bill_ID);
            }
            for (ReceiptDetail r : object.getReceiptDetail()) {
                String sqlDetail = "INSERT INTO bill_detail (Prod_ID,Bill_ID,BillDet_Price,BillDet_Amount) VALUES (?,?,?,?);";
                PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
                stmtDetail.setInt(1, r.getProduct().getProd_ID());
                stmtDetail.setInt(2, r.getReceipt().getRec_ID());
                stmtDetail.setDouble(3, r.getRecDet_Preice());
                stmtDetail.setInt(4, r.getRecDet_Amount());
                int rowDetail = stmtDetail.executeUpdate();
                ResultSet resultDetail = stmt.getGeneratedKeys();
                if (resultDetail.next()) {
                    Bill_ID = resultDetail.getInt(1);
                    r.setRecDet_ID(Bill_ID);
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error : Can't created Receipt");
        }
        db.close();
        return Bill_ID;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT b.Bill_ID as Bill_ID,Bill_Date,Cus_ID,Cus_Name,Cus_Tel,Emp_ID,Emp_Name,Emp_Tel,Bill_Total\n"
                    + "FROM bill b,customer c,employee e \n"
                    + "WHERE b.Cus_ID = c.Cus_ID AND b.Emp_ID = e.Emp_ID \n"
                    + "ORDER BY Bill_Date DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int Bill_ID = result.getInt("Bill_ID");
                Date Bill_Date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Bill_Date"));
                int Cus_ID = result.getInt("Cus_ID");
                String Cus_Name = result.getString("Cus_Name");
                String Cus_Tel = result.getString("Cus_Tel");
                int Emp_ID = result.getInt("Emp_ID");
                String Emp_Name = result.getString("Emp_Name");
                String Emp_Tel = result.getString("Emp_Tel");
                double Bill_Total = result.getDouble("Bill_Total");
                Receipt receipt = new Receipt(Bill_ID, Bill_Date, new Employee(Emp_ID, Emp_Name, Emp_Tel), new Customer(Cus_ID, Cus_Name, Cus_Tel));
                list.add(receipt);
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select all receipt");
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing Unable to select all receipt");
        }
        db.close();
        return list;
    }

    @Override
    public Receipt get(int Bill_ID) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT Bill_ID,\n"
                    + "       Bill_Date,\n"
                    + "       Cus_ID,\n"
                    + "       Cus_Name,\n"
                    + "       Cus_Tel,\n"
                    + "       Emp_ID,\n"
                    + "       Emp_Name,\n"
                    + "       Emp_Tel,\n"
                    + "       Bill_Total\n"
                    + "  FROM bill b,customer c,employee e\n"
                    + "  WHERE b._Bill_ID AND b.Cus_ID = c.Cus_ID AND b.Emp_ID = e.Emp_ID;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Bill_ID);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                int Bill_RID = result.getInt("Bill_ID");
                Date Bill_Date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("Bill_Date"));
                int Cus_ID = result.getInt("Cus_ID");
                String Cus_Name = result.getString("Cus_Name");
                String Cus_Tel = result.getString("Cus_Tel");
                int Emp_ID = result.getInt("Emp_ID");
                String Emp_Name = result.getString("Emp_Name");
                String Emp_Tel = result.getString("Emp_Tel");
                double Bill_Total = result.getDouble("Bill_Total");
                Receipt receipt = new Receipt(Bill_RID, Bill_Date, new Employee(Emp_ID, Emp_Name, Emp_Tel), new Customer(Cus_ID, Cus_Name, Cus_Tel));
                getReceiptDetail(conn, sql, Bill_ID, receipt);
                return receipt;
            }
        } catch (SQLException ex) {
            System.out.println("Error : Unable to select receipt" + Bill_ID + ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error : Date parsing Unable to select all receipt");
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, String sql, int Bill_ID, Receipt receipt) throws SQLException {
        String sqlDetail = "SELECT BillDet_ID,\n"
                + "       Prod_ID,\n"
                + "       Prod_Name,\n"
                + "       Prod_Price,\n"
                + "       Bill_ID,\n"
                + "       BillDet_Price,\n"
                + "       BillDet_Amount\n"
                + "  FROM bill_detail bd,product p\n"
                + "  WHERE Bill_ID = ? AND bd.Prod_ID = p.Prod_ID;";
        PreparedStatement stmtDetail = conn.prepareStatement(sql);
        stmtDetail.setInt(1, Bill_ID);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while (resultDetail.next()) {
            int BillDet_ID = resultDetail.getInt("BillDet_ID");
            int Prod_ID = resultDetail.getInt("Prod_ID");
            String Prod_Name = resultDetail.getString("Prod_Name");
            double Prod_Price = resultDetail.getDouble("Prod_Price");
            double BillDet_Price = resultDetail.getDouble("BillDet_Price");
            int BillDet_Amount = resultDetail.getInt("BillDet_Amount");
            Product product = new Product(Prod_ID, Prod_Name, Prod_Price);
            receipt.addReceiptDetail(BillDet_ID,product, BillDet_Amount,BillDet_Price);
        }
    }

    @Override
    public int delete(int Bill_ID) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE Prod_ID = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, Bill_ID);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error : Unable to delete receipt" + Bill_ID);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();

//        int row = 0;
//        try {
//            String sql = "UPDATE product SET Prod_Name = ?,Prod_Price = ? WHERE Prod_ID = ?;";
//               PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setString(1, object.getProd_Name());
//            stmt.setDouble(2, object.getProd_Price());
//            stmt.setInt(3, object.getProd_ID());
//            row = stmt.executeUpdate();
//            System.out.println("Affect Row " + row);
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
        //       db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1, "Green tea", 30);
        Product p2 = new Product(2, "Espresso", 30);
        Employee seller = new Employee(1, "Siravich Reancharean", "0617214444");
        Customer customer = new Customer(1, "Yaya", "0617216666");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 5);
        System.out.println(receipt);
        ReceiptDao dao = new ReceiptDao();
        System.out.println("Bill_ID = " + dao.add(receipt));
        System.out.println("Receipt after add " + receipt);
        System.out.println("Get All " + dao.getAll());

        Receipt newReceipt = dao.get(receipt.getRec_ID());
        System.out.println("New Receipt = " + newReceipt);
        
        dao.delete(receipt.getRec_ID());
    }

}
